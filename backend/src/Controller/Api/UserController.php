<?php

namespace App\Controller\Api;

use App\Form\UserType;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use App\Entity\User;

#[Route("/user", name: "user_")]
class UserController extends AbstractFOSRestController 
{
    #[Rest\Post("", name: "create")]
    public function create(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(UserType::class, new User());
        $form->submit($request->request->all());
        
        if (!$form->isValid()) {
            return $this->handleView(
                $this->view($form)
            );
        }
        
        $em->persist($form->getData());
        $em->flush();
        
        return $this->handleView(
            $this->view($form->getData(), Response::HTTP_CREATED, [])
        );
    }
    
    #[Rest\Get("/{id}", name: "read", requirements: ["id" => "\d+"])]
    public function read(int $id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->find($id);

        if (!$user) {
            throw new ResourceNotFoundException( "Resource $id not found");
        }
        
        return $this->handleView(
            $this->view($user, Response::HTTP_OK , [])
        );
    }
    
    #[Rest\Put("/{id}", name: "update", requirements: ["id" => "\d+"])]
    public function update(Request $request, int $id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->find($id);

        if (!$user) {
            throw new ResourceNotFoundException( "Resource $id not found");
        }
        
        $form = $this->createForm(UserType::class, $user);
        $form->submit($request->request->all());
        
        if (!$form->isValid()) {
            return $this->handleView(
                $this->view($form)
            );
        }
        
        $em->persist($user);
        $em->flush();
        
        return $this->handleView(
            $this->view($user, Response::HTTP_ACCEPTED , [])
        );
    }
    
    #[Rest\Delete("/{id}", name: "delete", requirements: ["id" => "\d+"])]
    public function delete(int $id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->find($id);

        if (!$user) {
            throw new ResourceNotFoundException( "Resource $id not found");
        }
        
        $em->remove($user);
        $em->flush();
        
        return $this->handleView(
            $this->view($user, Response::HTTP_NO_CONTENT , [])
        );
    }
}
