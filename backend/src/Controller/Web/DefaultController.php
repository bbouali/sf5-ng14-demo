<?php

namespace App\Controller\Web;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Description of DefaultController
 *
 * @author bbouali
 * 
 * @Route(
 *  "/{path}",
 *  name="index",
 *  requirements={"path"="^(?!api\/).*$"}
 * )
 */
class DefaultController extends AbstractController 
{
    public function __invoke()
    {
        return new Response(file_get_contents($this->getParameter('kernel.project_dir') . '/public/dist/index.html'));
    }
}
