<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * Description of CreateUserCommand
 *
 * @author bbouali
 */
#[AsCommand(
    name: 'app:create-user',
    description: 'Creates a new user.',
    hidden: false,
    aliases: ['app:add-user']
)]
class CreateUserCommand extends Command
{
    public function __construct(
            private EntityManagerInterface $em,
            private UserPasswordHasherInterface $passwordHasher,
            )
    {
        $this->em = $em;
        $this->passwordHasher = $passwordHasher;

        parent::__construct();
    }
    
    protected function configure(): void
    {
        $this
            ->setHelp('This command allows you to create a user...')
            ->addOption('username', 'u', InputOption::VALUE_REQUIRED, 'The username of the user.')
            ->addOption('password', 'p', InputOption::VALUE_REQUIRED, 'The password of the user.')
            ->addOption('isAdmin', 'a', InputOption::VALUE_NONE, 'The password of the user.')
        ;
    }
    
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln([
            'Create User Command START',
            '============',
            '',
            'Creating user '.$input->getOption('username').'...',
        ]);
        
        $user = new User();
        $user->setUsername($input->getOption('username'));
        if($input->getOption('isAdmin')){
            $user->setRoles(['ROLE_ADMIN']);
        }
        $hashedPassword = $this->passwordHasher->hashPassword(
            $user,
            $input->getOption('password')
        );
        $user->setPassword($hashedPassword);
        
        $this->em->persist($user);
        $this->em->flush();
        
        $output->writeln([
            'Created user '.$input->getOption('username').'!',
            '',
            '============',
            'Create User Command END',
        ]);
        return Command::SUCCESS;
    }
}
