export interface UserInterface {
  id?: number
  username: string;
  roles: string[];
}

export class User {
  public id?: number
  public username: string;
  public roles: string[];

  constructor(user: UserInterface) {
    if(user.id) {
      this.id = user.id;
    }
    this.username = user.username;
    this.roles = user.roles;
  }
}
