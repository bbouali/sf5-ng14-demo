import jwtDecode from "jwt-decode";
import * as moment from "moment";
import { User } from "./user";

export class SessionContext {

  public token: string;
  public user: User;
  public exp: number;

  constructor(token: string) {
    this.token = token;
    let data: any = jwtDecode(token);
    this.user = new User({username: data.username, roles: data.roles});
    // consider diff time between frontend & backend
    let diffTime = moment().unix() - data.iat;
    this.exp = data.exp + diffTime;
  }
}