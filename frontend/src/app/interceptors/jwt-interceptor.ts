import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { AuthService } from "../services/auth.service";
import { StorageService } from "../services/storage.service";

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

    constructor(
      private authService: AuthService,
      private storageService: StorageService,
      ) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      if (req.url.startsWith('/api/') &&
      !req.url.startsWith('/api/login_check') &&
      this.authService.isLoggedIn) {
        req = req.clone({
            setHeaders: {
                Authorization: "Bearer " + this.storageService.getSessionContext()?.token
            }
        });
      }

      return next.handle(req);
    }
}
