import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { catchError, Observable, throwError } from "rxjs";
import { AuthService } from "../services/auth.service";
import { StorageService } from "../services/storage.service";

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

    constructor(
      private router: Router, 
      private authService: AuthService,
      private storageService: StorageService,
      ) { }

      intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(req).pipe(catchError(err => {
          if ([401, 403].includes(err.status) && 
          !this.router.url.startsWith('/login')) {
              // auto logout if 401 or 403 response returned from api
              this.authService.logout();
              this.router.navigate(['']);
          }

          const error = (err && err.error && err.error.message) || err.statusText;
          return throwError(() => error);
        }));
      }
}
