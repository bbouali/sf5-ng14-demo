import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { StorageService } from './storage.service';
import { SessionContext } from '../models/session-context';
import jwtDecode from 'jwt-decode';
import * as moment from 'moment';
import { NgxPermissionsService, NgxRolesService } from 'ngx-permissions';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient,
    private storageService: StorageService,
    private roleService: NgxRolesService,
    ) { }

  public login(username: string, password: string): Observable<any> {
    this.logout();
    
    return this.http.post<any>(
      '/api/login_check',
      {
        username: username,
        password: password,
      },
    ).pipe(map(data => {
      if(data.hasOwnProperty('token')) {
        let sessionContext = new SessionContext(data.token);
        this.storageService.saveSessionContext(sessionContext);
        this.loadRoles();
      }

      return data;
    }));
  }

  public logout(): void {
    this.storageService.clear();
    this.roleService.flushRoles();
  }

  public get isLoggedIn(): boolean {
    let sessionStorage = this.storageService.getSessionContext();
    return (sessionStorage || false) && sessionStorage.exp >= moment().unix();
  }

  public loadRoles(): void {
    this.roleService.flushRoles();
    if(this.isLoggedIn) {
      this.storageService.getSessionContext()?.user.roles.forEach(role => this.roleService.addRole(role, []));
    }
  }
}
