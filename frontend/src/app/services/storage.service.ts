import { Injectable } from '@angular/core';
import { SessionContext } from '../models/session-context';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  clear(): void {
    localStorage.clear();
    sessionStorage.clear();
  }

  public saveSessionContext(sessionContext: SessionContext): void {
    localStorage.setItem('sessionContext', JSON.stringify(sessionContext));
  }

  public getSessionContext(): SessionContext | null {
    const sessionContext = localStorage.getItem('sessionContext');
    if (sessionContext) {
      return JSON.parse(sessionContext);
    }

    return null;
  }
}
